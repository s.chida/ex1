<?php
header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Спасибо, результаты сохранены.');
  }
  include('form.php');
  exit();
}


$errors = FALSE;
if (empty($_POST['fio'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}
if (empty($_POST['birthyear'])) {
  print('Заполните год рождения.<br/>');
  $errors = TRUE;
}
if (!isset($_POST['radio2'])) {
  print('Выберите пол.<br/>');
  $errors = TRUE;
}
if (!isset($_POST['radio1'])) {
  print('Выберите к-во конечностей.<br/>');
  $errors = TRUE;
}
if (!isset($_POST['checkbox'])) {
  print('Подтверите согласие.<br/>');
  $errors = TRUE;
}


if ($errors) {
  exit();
}


$user = 'u20990';
$pass = '7645415';
$db = new PDO('mysql:host=localhost;dbname=u20990', $user, $pass, array(PDO::ATTR_PERSISTENT => true));



$stmt = $db->prepare("INSERT INTO form (name, year,email,sex,limb,bio,checkbox) VALUES (:fio, :birthyear,:email,:sex,:limb,:bio,:checkbox)");
$stmt -> execute(array('fio'=>$_POST['fio'], 'birthyear'=>$_POST['birthyear'],'email'=>$_POST['email'],'sex'=>$_POST['radio2'],'limb'=>$_POST['radio1'],'bio'=>$_POST['textarea1'],'checkbox'=>$_POST['checkbox']));


$stmt=$db->prepare("INSERT INTO all_abilities(ability_god,ability_through_walls,ability_levity,ability_kostenko,ability_kolotiy) VALUES(:god,:wall,:levity,:kostenko,:kolotiy)");
$myselect=$_POST['select1'];
for($i=0;$i<5;$i++)
{
  if($myselect[$i]!=1)
  {
    $myselect[$i]=0;
  }
}
$stmt->execute(array('god'=>$myselect[0],'wall'=>$myselect[1],'levity'=>$myselect[2],'kostenko'=>$myselect[3],'kolotiy'=>$myselect[4]));


header('Location: ?save=1');
